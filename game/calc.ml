open Constants
open Definitions
open Util

let _ = Random.self_init()

(* Returns a (probability)% of the time, otherwise b *)
let choose probability a b =
  if (Random.float 1.0) *. 100.0 < float_of_int probability then a else b

let clamp x min max =
  if x < min then min else if x > max then max else x

let calculate_base_attack steammon element =
  let multiplier () =
    match clamp steammon.mods.attack_mod (-3) 3 with
    | -3 -> cATTACK_DOWN3
    | -2 -> cATTACK_DOWN2
    | -1 -> cATTACK_DOWN1
    | 0 -> 1.
    | 1 -> cATTACK_UP1
    | 2 -> cATTACK_UP2
    | _ -> cATTACK_UP3 in
  match element with
  | Fire|Water|Electric|Ghost|Psychic -> float_of_int steammon.spl_attack
  | _ -> (float_of_int steammon.attack) *. multiplier ()

let calculate_base_defense steammon element =
  let multiplier () =
    match clamp steammon.mods.defense_mod (-3) 3 with
    | -3 -> cDEFENSE_DOWN3
    | -2 -> cDEFENSE_DOWN2
    | -1 -> cDEFENSE_DOWN1
    | 0 -> 1.
    | 1 -> cDEFENSE_UP1
    | 2 -> cDEFENSE_UP2
    | _ -> cDEFENSE_UP3 in
  match element with
  | Fire|Water|Electric|Ghost|Psychic -> float_of_int steammon.spl_defense
  | _ -> (float_of_int steammon.defense) *. multiplier ()

let calculate_damage attacker defender attack confused crit =
  let attack_power = float_of_int attack.power in
  let attackers_attack = calculate_base_attack attacker attack.element in
  let stab_bonus =
    match attacker.first_type with
    | Some a -> if a = attack.element then cSTAB_BONUS else begin
      match attacker.second_type with
      | Some b -> if b = attack.element then cSTAB_BONUS else 1.0
      | None -> 1. end
    | None -> 1. (* this shouldn't ever happen *) in
  let steamtype_multiplier =
    let multiplier element =
      match element with
      | Some a -> weakness attack.element a
      | None -> 1. in
    (multiplier defender.first_type) *. (multiplier defender.second_type) in
  let defenders_defense = calculate_base_defense defender attack.element in
  match defenders_defense with
  | 0. -> failwith "divide by zero"
  | _ -> int_of_float (attack_power *. attackers_attack *. crit *.
         stab_bonus *. steamtype_multiplier /. defenders_defense)

let calculate_accuracy steammon attack =
  let multiplier =
    match clamp steammon.mods.accuracy_mod (-3) 3 with
    | -3 -> cACCURACY_DOWN3
    | -2 -> cACCURACY_DOWN2
    | -1 -> cACCURACY_DOWN1
    | 0 -> 1.
    | 1 -> cACCURACY_UP1
    | 2 -> cACCURACY_UP2
    | _ -> cACCURACY_UP3 in
  int_of_float ((float_of_int attack.accuracy) *. multiplier)

let calculate_speed steammon =
  let p_multiplier =
    if List.mem Paralyzed steammon.status then
      1. /. (float_of_int cPARALYSIS_SLOW)
    else 1. in
  let mod_multiplier =
    match clamp steammon.mods.speed_mod (-3) 3 with
    | -3 -> cSPEED_DOWN3
    | -2 -> cSPEED_DOWN2
    | -1 -> cSPEED_DOWN1
    | 0 -> 1.
    | 1 -> cSPEED_UP1
    | 2 -> cSPEED_UP2
    | _ -> cSPEED_UP3 in
  int_of_float ((float_of_int steammon.speed) *. mod_multiplier *. p_multiplier)