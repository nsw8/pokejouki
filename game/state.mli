open Definitions

type state = game_status_data

val blank_state : state

(* Returns the updated state by adding a steammon to the specified team. *)
val add_steammon :
  state -> steammon list -> color -> string -> state * bool

(* Returns the updated state by setting the inventory of the specified team. *)
val set_inventory : state -> color -> inventory -> state

(* Returns the updated state where the damage/status inflicted by attacker's
 * attack is applied to the current steammon of the specified team. *)
val apply_attack : state -> color -> string -> update list * state

(* Returns the updated state where item applied to the specified steammon of
 * the specified team. *)
val use_item : state -> color -> string -> item -> update list * state

(* Swaps the current steammon with a steammon at steammon_index for the
 * specified team, returning the updated state. *)
val switch_steammon : state -> color -> string -> bool -> state

(* Applies damage due to poisoning to each steammon and returns the updated
 * state. *)
val apply_poison_damage : state -> update list * state