open Definitions

type mutable_steammon = {
  mutable m_curr_hp: int;
  mutable m_status: status option * status option;
  mutable m_attack_mod: int;
  mutable m_speed_mod: int;
  mutable m_defense_mod: int;
  mutable m_accuracy_mod: int;
  mutable m_atk1_pp: int;
  mutable m_atk2_pp: int;
  mutable m_atk3_pp: int;
  mutable m_atk4_pp: int
  }

let pair_of_status_list lst =
  match lst with
    | [a;b] -> (Some a, Some b)
    | [Confused] -> (None, Some Confused)
    | [a] -> (Some a, None)
    | _ -> (None, None)

let extract_mutable_fields steammon = {
  m_curr_hp = steammon.curr_hp;
  m_status = pair_of_status_list steammon.status;
  m_attack_mod = steammon.mods.attack_mod;
  m_speed_mod = steammon.mods.speed_mod;
  m_defense_mod = steammon.mods.defense_mod;
  m_accuracy_mod = steammon.mods.accuracy_mod;
  m_atk1_pp = steammon.first_attack.pp_remaining;
  m_atk2_pp = steammon.second_attack.pp_remaining;
  m_atk3_pp = steammon.third_attack.pp_remaining;
  m_atk4_pp = steammon.fourth_attack.pp_remaining
  }

let merge_updated_steammon_fields steammon mutable_steammon =
  let updated_mods = {
    attack_mod = mutable_steammon.m_attack_mod;
    speed_mod = mutable_steammon.m_speed_mod;
    defense_mod = mutable_steammon.m_defense_mod;
    accuracy_mod = mutable_steammon.m_accuracy_mod;
    } in
  let update_atk atk new_pp = {
    name = atk.name;
    element = atk.element;
    max_pp = atk.max_pp;
    pp_remaining = new_pp;
    power = atk.power;
    accuracy = atk.accuracy;
    crit_chance = atk.crit_chance;
    effect = atk.effect
    } in
  let atk1 = update_atk steammon.first_attack mutable_steammon.m_atk1_pp in
  let atk2 = update_atk steammon.second_attack mutable_steammon.m_atk2_pp in
  let atk3 = update_atk steammon.third_attack mutable_steammon.m_atk3_pp in
  let atk4 = update_atk steammon.fourth_attack mutable_steammon.m_atk4_pp in {
    species = steammon.species;
    curr_hp = max mutable_steammon.m_curr_hp 0;
    max_hp = steammon.max_hp;
    first_type = steammon.first_type;
    second_type = steammon.second_type;
    first_attack = atk1;
    second_attack = atk2;
    third_attack = atk3;    
    fourth_attack = atk4;
    attack = steammon.attack;
    spl_attack = steammon.spl_attack;
    defense = steammon.defense;
    spl_defense = steammon.spl_defense;
    speed = steammon.speed;
    status = begin
      match mutable_steammon.m_status with
      | (Some a, Some b) -> [a;b]
      | (Some a, _) -> [a]
      | (_, Some b) -> [b]
      | (_, _) -> [] end;
    mods = updated_mods
  }