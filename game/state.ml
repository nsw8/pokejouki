open Definitions
open Data
open Util
open Constants
open Mutables
open Calc

type state = game_status_data

let blank_state = (([],[]),([],[]))

let add_steammon state steammon_list team_color steammon =
  let update_state ((red_team, red_items), (blue_team, blue_items)) =
    let a = List.find (fun a -> a.species = steammon) steammon_list in
    if not (List.mem a red_team) && not (List.mem a blue_team) then
      match team_color with
      | Red -> (((a::red_team, red_items), (blue_team, blue_items)), true)
      | Blue -> (((red_team, red_items), (a::blue_team, blue_items)), true)
    else (state, false) in
  update_state state

let set_inventory state team_color inventory =
  let is_valid = ref (List.length inventory = 8) in
  let cost =
    let update_cost acc item cost =
      if item < 0 then is_valid := false else ();
      acc + item * cost in
    List.fold_left2 update_cost 0 inventory item_cost_list in
  let update_state ((red_team, red_items), (blue_team, blue_items)) inventory =
    match team_color with
    | Red -> ((red_team, inventory), (blue_team, blue_items))
    | Blue -> ((red_team, red_items), (blue_team, inventory)) in
  if cost <= cINITIAL_CASH && !is_valid then update_state state inventory
  else update_state state default_items

let apply_attack state team_color attack =
  let (attacker, defender) =
    match state with
    | ((hr::tr, _), (hb::tb, _)) -> begin
        match team_color with
        | Red -> (hr, hb)
        | Blue -> (hb, hr) end
    | _, _ -> failwith "some team doesn't have any steammon" in
  let message = Message ("> " ^ attacker.species ^ " used " ^ attack) in
  let (atk_id, atk) =
    if attack = attacker.first_attack.name then (1,attacker.first_attack)
    else if attack = attacker.second_attack.name then (2,attacker.second_attack)
    else if attack = attacker.third_attack.name then (3,attacker.third_attack)
    else if attack = attacker.fourth_attack.name then (4,attacker.fourth_attack)
    else (0, attacker.first_attack) in
  let resolve_status_message = ref None in
  let updated_status =
    let (p, c) = pair_of_status_list attacker.status in
    let resolve_status status =
      match status with
      | Some Asleep ->
        let resolved_status = choose cWAKE_UP_CHANCE None status in
        resolve_status_message :=
          Some (Message ("> " ^ attacker.species ^ " woke up!"));
        resolved_status
      | Some Confused ->
        let resolved_status = choose cSNAP_OUT_OF_CONFUSION None status in
        resolve_status_message :=
          Some (Message ("> " ^ attacker.species ^ " is no longer confused!"));
        resolved_status
      | Some Frozen ->
        let resolved_status = choose cDEFROST_CHANCE None status in
        resolve_status_message :=
          Some (Message ("> " ^ attacker.species ^ " was unfrozen!"));
        resolved_status
      | _ -> status in
    (resolve_status p, resolve_status c) in
  let confused =
    match snd updated_status with
    | Some Confused -> true
    | _ -> false in
  let attack_failure =
    match fst updated_status with
    | Some Asleep | Some Frozen -> true
    | Some Paralyzed -> choose cPARALYSIS_CHANCE true false
    | _ -> atk.pp_remaining <= 0 in
  let attack_miss =
    choose (calculate_accuracy attacker atk) false true in
  let m_attacker = extract_mutable_fields attacker in
  let m_defender = extract_mutable_fields defender in
  let _ = m_attacker.m_status <- updated_status in
  let eval_message =
    if attack_failure || atk_id = 0 then
      [NegativeEffect("Can't attack!", team_color, 0)]
    else begin
      let _ =
        match atk_id with
        | 1 -> m_attacker.m_atk1_pp <- m_attacker.m_atk1_pp - 1
        | 2 -> m_attacker.m_atk2_pp <- m_attacker.m_atk2_pp - 1
        | 3 -> m_attacker.m_atk3_pp <- m_attacker.m_atk3_pp - 1
        | _ -> m_attacker.m_atk4_pp <- m_attacker.m_atk4_pp - 1 in
      if attack_miss then
        [NegativeEffect("Attack missed", invert_color team_color, 0)]
      else
        let effective =
          match defender.first_type, defender.second_type with
          | Some x, Some y -> (weakness atk.element x) *.
                              (weakness atk.element y)
          | Some x, None -> (weakness atk.element x)
          | _, _ -> 1. in
        let crit = choose atk.crit_chance cCRIT_MULTIPLIER 1.0 in
        let damage =
          let defender = if confused then attacker else defender in
          calculate_damage attacker defender atk confused crit in
        let apply_effect () =
          let (eff, chance) = atk.effect in
          let (s1, c) = m_defender.m_status in
          let other_team = invert_color team_color in
          let default_effect =
            NegativeEffect("Attack hit", other_team, damage) in
          let special_effect() =
            match eff with
            | Nada -> [default_effect]
            | Poisons -> begin
              match s1 with None -> m_defender.m_status <- (Some Poisoned, c)
              | _ -> () end;
              [NegativeEffect("Poisoned!", other_team, damage)]
            | Confuses ->
              m_defender.m_status <- (s1, Some Confused);
              [NegativeEffect("Confused!", other_team, damage)]
            | Sleeps -> begin
              match s1 with None -> m_defender.m_status <- (Some Asleep, c)
              | _ -> () end;
              [NegativeEffect("Put to sleep!", other_team, damage)]
            | Paralyzes -> begin
              match s1 with None -> m_defender.m_status <- (Some Paralyzed, c)
              | _ -> () end;
              [NegativeEffect("Paralyzed!", other_team, damage)]
            | Freezes -> begin
              match s1 with None -> m_defender.m_status <- (Some Frozen, c)
              | _ -> () end;
              [NegativeEffect("Frozen!", other_team, damage)]
            | SelfAttackUp1 ->
              m_attacker.m_attack_mod <- m_attacker.m_attack_mod + 1;
              let m = PositiveEffect("Attack up!", team_color, 0) in begin
              match atk.power with 0 -> [m] | _ -> [m;default_effect] end
            | SelfDefenseUp1 ->
              m_attacker.m_defense_mod <- m_attacker.m_defense_mod + 1;
              let m = PositiveEffect("Defense up!", team_color, 0) in begin
              match atk.power with 0 -> [m] | _ -> [m;default_effect] end
            | SelfSpeedUp1 ->
              m_attacker.m_speed_mod <- m_attacker.m_speed_mod + 1;
              let m = PositiveEffect("Speed up!", team_color, 0) in begin
              match atk.power with 0 -> [m] | _ -> [m;default_effect] end
            | SelfAccuracyUp1 ->
              m_attacker.m_accuracy_mod <- m_attacker.m_accuracy_mod + 1;
              let m = PositiveEffect("Accuracy up!", team_color, 0) in begin
              match atk.power with 0 -> [m] | _ -> [m;default_effect] end
            | OpponentAttackDown1 ->
              m_defender.m_attack_mod <- m_defender.m_attack_mod - 1;
              [NegativeEffect("Attack down!", other_team, damage)]
            | OpponentDefenseDown1 ->
              m_defender.m_defense_mod <- m_defender.m_defense_mod - 1;
              [NegativeEffect("Defense down!", other_team, damage)]
            | OpponentSpeedDown1 ->
              m_defender.m_speed_mod <- m_defender.m_speed_mod - 1;
              [NegativeEffect("Speed down!", other_team, damage)]
            | OpponentAccuracyDown1 ->
              m_defender.m_accuracy_mod <- m_defender.m_accuracy_mod - 1;
              [NegativeEffect("Accuracy down!", other_team, damage)] in
          choose chance (special_effect()) [default_effect] in
        if confused then
          (m_attacker.m_curr_hp <- m_attacker.m_curr_hp - damage;
          [NegativeEffect("Attacked self!", team_color, damage)])
        else
          let messages =
            let intermediate =
              let primary = apply_effect() in
              if effective > 1. && damage > 0 then
                (Message "> It's super effective!")::primary
              else if effective < 1. && damage > 0 then
                if effective = 0. then
                  (Message "> It's not effective at all...")::primary
                else
                  (Message "> It's not very effective...")::primary
              else primary in
            if crit > 1. && damage > 0 then
              (Message "> Critical hit!")::intermediate else intermediate in
          (m_defender.m_curr_hp <- m_defender.m_curr_hp - damage;
          messages) end in
  let updated_state =
    let updated_attacker =
      merge_updated_steammon_fields attacker m_attacker in
    let updated_defender =
      merge_updated_steammon_fields defender m_defender in
    match state with
    | ((hr::tr, ri), (hb::tb, bi)) -> begin
        match team_color with
        | Red -> ((updated_attacker::tr, ri), (updated_defender::tb, bi))
        | Blue -> ((updated_defender::tr, ri), (updated_attacker::tb, bi)) end
    | _, _ -> failwith "this error never occurs" in
  let gui_updates =
    let m = message::eval_message in
    match !resolve_status_message with
    | Some x -> x::m
    | None -> m in
  (gui_updates, updated_state)
  

let use_item state team_color steammon item =
  let team =
    match team_color with
      Red -> fst state
    | Blue -> snd state in
  let ini_message =
    Message ("> " ^ string_of_color team_color ^ " used " ^
             string_of_item item ^ " on " ^
             steammon) in
  let (updated_items, success) =
    match snd team with
    | [a;b;c;d;e;f;g;h] -> begin
      let (candidate, item_no) =
        match item with
        | Ether -> ([a-1;b;c;d;e;f;g;h], a)
        | MaxPotion -> ([a;b-1;c;d;e;f;g;h], b)
        | Revive -> ([a;b;c-1;d;e;f;g;h], c)
        | FullHeal -> ([a;b;c;d-1;e;f;g;h], d)
        | XAttack -> ([a;b;c;d;e-1;f;g;h], e)
        | XDefense -> ([a;b;c;d;e;f-1;g;h], f)
        | XSpeed -> ([a;b;c;d;e;f;g-1;h], g)
        | XAccuracy -> ([a;b;c;d;e;f;g;h-1], h) in
      if item_no > 0 then (candidate, true) else (snd team, false) end
    | _ -> failwith "incorrect inventory format" in
  let message = ref " " in
  let hp_gained = ref 0 in
  let apply_item steammon =
    let m_steammon = extract_mutable_fields steammon in
    let _ =
      match item with
        Ether ->
          message := "pp recovered";
          m_steammon.m_atk1_pp <-
            min (steammon.first_attack.pp_remaining + 5)
            steammon.first_attack.max_pp;
          m_steammon.m_atk2_pp <-
            min (steammon.second_attack.pp_remaining + 5)
            steammon.second_attack.max_pp;
          m_steammon.m_atk3_pp <-
            min (steammon.third_attack.pp_remaining + 5)
            steammon.third_attack.max_pp;
          m_steammon.m_atk4_pp <-
            min (steammon.fourth_attack.pp_remaining + 5)
            steammon.fourth_attack.max_pp
      | MaxPotion ->
        hp_gained := steammon.max_hp - m_steammon.m_curr_hp;
        message := "hp recovered";
        m_steammon.m_curr_hp <- steammon.max_hp
      | Revive ->
        if steammon.curr_hp <= 0 then
          (message := "steammon revived";
          hp_gained := steammon.max_hp / 2;
          m_steammon.m_curr_hp <- steammon.max_hp / 2;
          m_steammon.m_status <- (None, None))
        else () (* Pokemon has not fainted *)
      | FullHeal ->
        message := "status cleared";
        m_steammon.m_status <- (None, None)
      | XAttack ->
        message := "attack raised";
        m_steammon.m_attack_mod <- steammon.mods.attack_mod + 1
      | XDefense ->
        message := "defense raised";
        m_steammon.m_defense_mod <- steammon.mods.defense_mod + 1
      | XSpeed ->
        message := "speed raised";
        m_steammon.m_speed_mod <- steammon.mods.speed_mod + 1
      | XAccuracy ->
        message := "accuracy raised";
        m_steammon.m_accuracy_mod <-steammon.mods.accuracy_mod+1 in
    m_steammon in
  let rec apply_to_steammon lst =
    let c_steammon =
      match item with
        XAttack | XDefense | XSpeed | XAccuracy -> begin
          match lst with
            [] -> failwith "team doesn't have any steammon"
          | h::t -> h.species end
        | _ -> steammon in
    match lst with
      [] -> []
    | h::t ->
      if h.species = c_steammon then
        (merge_updated_steammon_fields h (apply_item h))::t
      else h::(apply_to_steammon lst) in
  let updated_team () = (apply_to_steammon (fst team), updated_items) in
  let updated_state =
    if success then
      match team_color with
      | Red -> ((updated_team(), snd state))
      | Blue -> ((fst state, updated_team()))
    else state in
  let gui_updates =
    if success then
      ini_message::[PositiveEffect (!message, team_color, !hp_gained)]
    else ini_message::[Message ("> Item had no effect!")] in
  (gui_updates, updated_state)

let switch_steammon state team_color steammon starter =
  let steammon_list =
    match team_color with
      Red -> fst (fst state)
    | Blue -> fst (snd state) in
  let (head, tail) =
    match steammon_list with
    | [] -> failwith "team doesn't have any steammon"
    | h::t -> (h, t) in
  let (replacement, residual) =
    let (a, b) =
      List.partition (fun a -> a.species = steammon) steammon_list in
    match a with
    | [x] ->
      if x.curr_hp > 0 then (a, b)
      else if not starter then ([], [])
      else
        let f acc element = if element.curr_hp > 0 then Some element else acc in
        begin match List.fold_left f None tail with
        | Some steammon -> List.partition (fun a -> a = steammon) steammon_list
        | None -> ([], []) end
    | _ -> (a, b) in
  let updated_team =
    match replacement with
    | [a] ->
      if (a.species = head.species) then steammon_list
      else begin
        match residual with
        | [] -> steammon_list (* There is only one steammon *)
        | head::tail ->
          let m_head = extract_mutable_fields head in
          let _ = m_head.m_status <- (fst m_head.m_status, None) in
          let _ = m_head.m_attack_mod <- 0 in
          let _ = m_head.m_defense_mod <- 0 in
          let _ = m_head.m_speed_mod <- 0 in
          let _ = m_head.m_accuracy_mod <- 0 in
          a::(merge_updated_steammon_fields head m_head)::tail end
    | _ -> steammon_list in
  match team_color with
    Red -> ((updated_team, (snd (fst state))), snd state)
  | Blue -> (fst state, (updated_team, (snd (snd state))))

let apply_poison_damage state =
  let ((red_steammon, red_items), (blue_steammon, blue_items)) = state in
  let (red_head, red_tail) =
    match red_steammon with h::t -> (h, t) | _ -> failwith "invalid" in
  let (blue_head, blue_tail) =
    match blue_steammon with h::t -> (h, t) | _ -> failwith "invalid" in
  let messages = ref [] in
  let update steammon team =
    let m_steammon = extract_mutable_fields steammon in
    let _ =
      match m_steammon.m_status with
      | (Some Poisoned, _) ->
        let damage = steammon.max_hp / 32 in
        (m_steammon.m_curr_hp <- m_steammon.m_curr_hp - damage;
        messages := [Message ("> " ^ steammon.species ^ " is poisoned!");
                     NegativeEffect ("Poisoned", team, damage)])
      | _ -> () in
    merge_updated_steammon_fields steammon m_steammon in
  let updated_state = 
    (((update red_head Red)::red_tail, red_items),
     ((update blue_head Blue)::blue_tail, blue_items)) in
  (!messages, updated_state)