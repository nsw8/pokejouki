open Definitions
open Util
open Constants
open Netgraphics
open State
open Data
open Calc

let first_pick =
  if Random.bool () then Red else Blue

let handle_pick_steammon g ra ba =
  let ind = if first_pick = Red then 0 else 1 in
  let ((updated_state, success), chosen_color) =
    let ((red_steammon, _), (blue_steammon, _)) = g in
    let red_num = List.length red_steammon in
    let blue_num = List.length blue_steammon in
    let len = List.length steammon_list in
    if red_num < blue_num || (red_num = blue_num && red_num mod 2 = ind) then
      match ra with
      | Action (PickSteammon steammon) ->
        (add_steammon g steammon_list Red steammon, Red)
      | _ ->
       let random_steammon = List.nth steammon_list (Random.int len) in
       (add_steammon g steammon_list Red random_steammon.species, Red)
    else
      match ba with
      | Action (PickSteammon steammon) ->
        (add_steammon g steammon_list Blue steammon, Blue)
      | _ ->
       let rec random_steammon() =
         let chosen = (List.nth steammon_list (Random.int len)).species in
         if List.exists (fun a -> a.species = chosen) red_steammon &&
            List.exists (fun a -> a.species = chosen) blue_steammon then
           chosen
         else random_steammon() in
       (add_steammon g steammon_list Blue (random_steammon()), Blue) in
  let ((red_steammon, _), (blue_steammon, _)) = updated_state in
  let _ = if success then
    match chosen_color with
    | Red -> begin
      match red_steammon with
      | h::_ -> add_update (UpdateSteammon(h.species,h.curr_hp,h.max_hp,Red))
      | _ -> () end
    | Blue -> begin
      match blue_steammon with
      | h::_ -> add_update (UpdateSteammon(h.species,h.curr_hp,h.max_hp,Blue))
      | _ -> () end else () in
  let red_num = List.length red_steammon in
  let blue_num = List.length blue_steammon in
  if red_num = blue_num && red_num = cNUM_PICKS then
    (None, updated_state,
     Some (Request (PickInventoryRequest updated_state)),
     Some (Request (PickInventoryRequest updated_state)))
  else if red_num < blue_num || (red_num = blue_num && red_num mod 2 = ind) then
    (None, updated_state,
     Some (Request (PickRequest(Red,updated_state,attack_list,steammon_list))),
     None)
  else
    (None, updated_state,
     None,
     Some (Request (PickRequest(Blue,updated_state,attack_list,steammon_list))))

let handle_pick_inventory g ra ba =
  let (ri, bi) =
    let get_requested_inventory action =
      match action with Action (PickInventory i) -> i | _ -> default_items in
    (get_requested_inventory ra, get_requested_inventory ba) in
  let intermediate_state = set_inventory g Red ri in
  let updated_state = set_inventory intermediate_state Blue bi in
  let _ = add_update (Message "> Teams have picked their items!") in
  (None, updated_state,
   Some (Request (StarterRequest updated_state)),
   Some (Request (StarterRequest updated_state)))

let update_steammon_gui red_starter blue_starter =
  add_update (UpdateSteammon (red_starter.species, red_starter.curr_hp,
                              red_starter.max_hp, Red));
  add_update (UpdateSteammon (blue_starter.species, blue_starter.curr_hp,
                              blue_starter.max_hp, Blue));
  add_update (SetStatusEffects (red_starter.species, red_starter.status));
  add_update (SetStatusEffects (blue_starter.species, blue_starter.status))

let end_turn g =
  let (updates, updated_state) = apply_poison_damage g in
  let _ = List.iter add_update updates in
  let ((red_steammon, _), (blue_steammon, _)) = updated_state in
  let (red_starter, blue_starter) =
    match red_steammon, blue_steammon with
    | h1::_, h2::_ -> (h1, h2)
    | _, _ -> failwith "team should have some steammon" in
  let _ = update_steammon_gui red_starter blue_starter in
  let helper acc a = a.curr_hp > 0 || acc in
  let red_loss = not (List.fold_left helper false red_steammon) in
  let blue_loss = not (List.fold_left helper false blue_steammon) in
  let (red_request, blue_request) =
    let red_hp = max red_starter.curr_hp 0 in
    let blue_hp = max blue_starter.curr_hp 0 in
    match red_hp, blue_hp with
    | 0, 0 -> (Some (Request (StarterRequest updated_state)),
               Some (Request (StarterRequest updated_state)))
    | 0, _ -> (Some (Request (StarterRequest updated_state)), None)
    | _, 0 -> (None, Some (Request (StarterRequest updated_state)))
    | _, _ -> (Some (Request (ActionRequest updated_state)),
               Some (Request (ActionRequest updated_state))) in
  let result =
    if red_loss then (if blue_loss then Some Tie else Some (Winner Blue))
    else if blue_loss then Some (Winner Red) else None in
  (result, updated_state, red_request, blue_request)

let handle_action g ra ba =
  let faster_team =
    match ra, ba with
    | Action (UseAttack _), Action (UseAttack _) ->
      let ((red_steammon, _), (blue_steammon, _)) = g in
      let (red_starter, blue_starter) =
        match red_steammon, blue_steammon with
        | h1::_, h2::_ -> (h1, h2)
        | _, _ -> failwith "team should have some steammon" in
      let blue_minus_red = (calculate_speed blue_starter) -
                           (calculate_speed red_starter) in
      if blue_minus_red > 0 then Blue
      else if blue_minus_red < 0 then Red
      else if Random.bool() then Red else Blue
    | Action (UseAttack _), _  | _, Action (SelectStarter _)-> Blue
    | _, _ -> Red in
  let _ = add_update (SetFirstAttacker (faster_team)) in
  let (fa, sa) =
    if faster_team = Red then (ra, ba) else (ba, ra) in
  let update_state state action team =
    match action with
    | Action (SelectStarter starter) ->
      let updated_state = switch_steammon state team starter true in
      let _ =
        add_update(
          Message("> " ^ string_of_color team ^ " chose " ^ starter ^ "!")) in
      let _ = add_update (SetChosenSteammon starter) in
      updated_state
    | Action (UseAttack attack) ->
      let (updates, updated_state) =
        apply_attack state team attack in
      let _ = List.iter add_update updates in
      updated_state
    | Action (UseItem (item, steammon)) ->
      let (updates, updated_state) =
        use_item state team steammon item in
      let _ = List.iter add_update updates in
      updated_state
    | Action (SwitchSteammon steammon) ->
      let updated_state = switch_steammon state team steammon false in
      let _ =
        let message = "> " ^ string_of_color team ^
                      " switched out " ^ steammon ^ "!" in
        add_update (Message (message)) in
      let _ = add_update (SetChosenSteammon steammon) in
      updated_state
    | _ -> state in
  let i_state = update_state g fa faster_team in
  let updated_state =
    let ((red_steammon, _), (blue_steammon, _)) = i_state in
    let (red_starter, blue_starter) =
      match red_steammon, blue_steammon with
      | h1::_, h2::_ -> (h1, h2)
      | _, _ -> failwith "team should have some steammon" in
    let _ = update_steammon_gui red_starter blue_starter in
    let slower_team = invert_color faster_team in
    let can_act =
      let slower_steammon =
        let slower_list =
          match slower_team with
          | Red -> (fst (fst i_state))
          | Blue -> (fst (snd i_state)) in
        match slower_list with
        | h::_ -> h
        | _ -> failwith "team should have some steammon" in
      slower_steammon.curr_hp > 0 in
    if can_act then
      update_state i_state sa slower_team
    else i_state in
  end_turn updated_state