open Definitions
open Util
open Constants
open Netgraphics
open State
open Data
open Actions

type game = state

let game_datafication g =
  g

let game_from_data game_data =
  game_data

let handle_step g ra ba =
  let ((red_steammon, red_inventory), (blue_steammon, _)) = g in
  let red_num = List.length red_steammon in
  let blue_num = List.length blue_steammon in
  let initiated_inventory = match red_inventory with
  | [] -> false
  | _ -> true in
  if not (red_num = blue_num && red_num = cNUM_PICKS) then
    handle_pick_steammon g ra ba
  else if not initiated_inventory then
    handle_pick_inventory g ra ba
  else
    handle_action g ra ba

let init_game () =
  send_update InitGraphics;
  (blank_state, first_pick, attack_list, steammon_list)
