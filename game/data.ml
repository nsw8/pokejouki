open Constants
open Definitions
open Util

let item_cost_list = [
  cCOST_ETHER;
  cCOST_MAXPOTION;
  cCOST_REVIVE;
  cCOST_FULLHEAL;
  cCOST_XATTACK;
  cCOST_XDEFEND;
  cCOST_XSPEED;
  cCOST_XACCURACY
  ]

let default_items =
  [cNUM_ETHER;cNUM_MAX_POTION;cNUM_REVIVE;cNUM_FULL_HEAL;
   cNUM_XATTACK;cNUM_XDEFENSE;cNUM_XACCURACY;cNUM_XSPEED]

let attack_hashtbl =
  let attack_strings =
    match read_lines "attack.txt" with
      [] -> []
    | h::t -> if h.[0] = '(' then t else h::t in
  let hashtbl = Table.create 100 in
  let add_to_hashtbl str =
    match wordify str with
      [name;element;pp;power;accuracy;crit_chance;effect_num;effect_chance] ->
        let attack = {
          name = name;
          element = type_of_string element;
          max_pp = int_of_string pp;
          pp_remaining = int_of_string pp;
          power = int_of_string power;
          accuracy = int_of_string accuracy;
          crit_chance = int_of_string crit_chance;
          effect =
            (effect_of_num (int_of_string effect_num),
            int_of_string effect_chance)
          } in
        Table.add hashtbl name attack
    | _ -> failwith "incorrect input" in
  let _ = List.iter add_to_hashtbl attack_strings in
  hashtbl

let attack_list =
  hash_to_list attack_hashtbl

let steammon_list =
  let empty_mods = {
    attack_mod = 0;
    speed_mod = 0;
    defense_mod = 0;
    accuracy_mod = 0
    } in
  let steammon_strings = read_lines "steammon.txt" in
  let steammon_from_string str =
    match wordify str with
      [name;hp;type1;type2;atk1;atk2;atk3;atk4;atk;sp_atk;def;sp_def;speed] -> {
          species = name;
          curr_hp = int_of_string hp;
          max_hp = int_of_string hp;
          first_type =
            if (type1 = "Nothing") then None
            else Some (type_of_string type1);
          second_type =
            if (type2 = "Nothing") then None
            else Some (type_of_string type2);
          first_attack = Table.find attack_hashtbl atk1;
          second_attack = Table.find attack_hashtbl atk2;
          third_attack = Table.find attack_hashtbl atk3;
          fourth_attack = Table.find attack_hashtbl atk4;
          attack = int_of_string atk;
          spl_attack = int_of_string sp_atk;
          defense = int_of_string def;
          spl_defense = int_of_string sp_def;
          speed = int_of_string speed;
          status = [];
          mods = empty_mods
          }
    | _ -> failwith "incorrect input" in
  List.map steammon_from_string steammon_strings

let steammon_hashtbl =
  let empty_mods = {
    attack_mod = 0;
    speed_mod = 0;
    defense_mod = 0;
    accuracy_mod = 0
    } in
  let steammon_strings = read_lines "steammon.txt" in
  let hashtbl = Table.create 100 in
  let add_to_hashtbl str =
    match wordify str with
      [name;hp;type1;type2;atk1;atk2;atk3;atk4;atk;sp_atk;def;sp_def;speed] ->
        let steammon = {
          species = name;
          curr_hp = int_of_string hp;
          max_hp = int_of_string hp;
          first_type =
            if (type1 = "Nothing") then None
            else Some (type_of_string type1);
          second_type =
            if (type2 = "Nothing") then None
            else Some (type_of_string type2);
          first_attack = Table.find attack_hashtbl atk1;
          second_attack = Table.find attack_hashtbl atk2;
          third_attack = Table.find attack_hashtbl atk3;
          fourth_attack = Table.find attack_hashtbl atk4;
          attack = int_of_string atk;
          spl_attack = int_of_string sp_atk;
          defense = int_of_string def;
          spl_defense = int_of_string sp_def;
          speed = int_of_string speed;
          status = [];
          mods = empty_mods
          } in
        Table.add hashtbl name steammon
    | _ -> failwith "incorrect input" in
  let _ = List.iter add_to_hashtbl steammon_strings in
  hashtbl
