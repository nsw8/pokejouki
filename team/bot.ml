open Team
open Definitions
open Constants

let _ = Random.self_init ()

let opponents_steammon = ref []

let all_types = [Normal; Fire; Water; Grass; Electric; Ice;
                 Fighting; Poison; Ground; Flying; Psychic;
                 Bug; Rock; Ghost; Dragon; Dark; Steel]

let spl_attack_types = [Electric; Fire; Water; Psychic; Ghost]

(* To help types play nice in other code...bad default though... *)
let desome_type t =
  match t with
  | Some x -> x
  | None -> Normal

(* get_weaknesses type1 returns a list of types that type1 is weak against *)
let get_weaknesses type1 =
  match type1 with
  | Normal-> [Fighting]     
  | Fire -> [Water; Ground; Rock]     
  | Water -> [Grass; Electric]
  | Grass -> [Fire; Ice; Poison; Flying; Bug]
  | Electric -> [Ground]
  | Ice -> [Fire; Fighting; Rock; Steel]
  | Fighting -> [Flying; Psychic]
  | Poison -> [Ground; Psychic]
  | Ground -> [Water; Grass; Ice]
  | Flying -> [Electric; Ice; Rock]
  | Psychic -> [Bug; Ghost; Dark]
  | Bug -> [Fire; Flying; Rock]
  | Rock -> [Water; Grass; Fighting; Ground; Steel]
  | Ghost -> [Ghost; Dark]
  | Dragon -> [Ice; Dragon]
  | Dark -> [Fighting; Bug]
  | Steel -> [Fire; Fighting; Ground]

(* get_strengths type1 returns a list of types that type1 is strong against *)
let get_strengths type1 =
  match type1 with
  | Normal-> []     
  | Fire -> [Grass; Ice; Bug; Steel]     
  | Water -> [Fire; Ground; Rock]
  | Grass -> [Ground; Rock]
  | Electric -> [Water; Flying]
  | Ice -> [Grass; Ground; Flying; Dragon]
  | Fighting -> [Normal; Ice; Rock; Dark; Steel]
  | Poison -> [Grass]
  | Ground -> [Fire; Electric; Poison; Rock; Steel]
  | Flying -> [Grass; Fighting; Bug]
  | Psychic -> [Fighting; Poison]
  | Bug -> [Grass; Psychic; Dark]
  | Rock -> [Fire; Ice; Ground; Bug]
  | Ghost -> [Psychic; Ghost]
  | Dragon -> [Dragon]
  | Dark -> [Psychic; Ghost]
  | Steel -> [Ice; Rock]
  
(* get_ineffectives type1 returns list of types that type1 is ineffective vs *)
let get_ineffectives type1 =
  match type1 with
  | Normal-> [Ghost]     
  | Fire -> []
  | Water -> []
  | Grass -> []
  | Electric -> [Ground]
  | Ice -> []
  | Fighting -> [Ghost]
  | Poison -> [Steel]
  | Ground -> [Flying]
  | Flying -> []
  | Psychic -> [Dark]
  | Bug -> []
  | Rock -> []
  | Ghost -> [Normal]
  | Dragon -> []
  | Dark -> []
  | Steel -> []

(* returns the steammon's four attacks *)
let get_attacks s =
  [s.first_attack; s.second_attack; s.third_attack; s.fourth_attack]

(* returns steammon that just got picked versus what we already knew *)
let get_new_steammon old_list gsd c =
  let progress = ref [] in
  let updated =
    match c with
    | Red -> fst (snd gsd)
    | Blue -> fst (fst gsd)
  in
  let one_stm_op s1 =
    let check = List.mem s1 old_list in
    if (not check) then
      (progress := s1::!progress;)
    else 
      (progress := !progress;)
  in
  List.iter one_stm_op updated;
  !progress

(* returns a list of types that are strong against types in the given list *)
let valid_type_choices opp_steammon_list =
  if (List.length opp_steammon_list = 0) then all_types
  else
    let valid_choices = ref [] in
    let one_stm stm =
      let type1 = stm.first_type in
      let type2 = stm.second_type in
      let weak1 = get_weaknesses (desome_type type1) in
      let weak2 = get_weaknesses (desome_type type2) in
      let merged = List.append weak1 weak2 in (valid_choices := List.append !valid_choices merged;)
    in
    List.iter one_stm opp_steammon_list;
    let unique l =
      let un_set = Hashtbl.create (List.length l) in
      List.iter (fun x -> Hashtbl.replace un_set x ()) l;
      Hashtbl.fold (fun x () xs -> x::xs) un_set []
    in unique !valid_choices
  
(* returns all available steammon of types in type_list *)
let steammon_of_types type_list stm_pool =
  if (List.length type_list = 0) then []
  else
  let stm_temp = ref [] in
  let helper s =
    let type1 = desome_type s.first_type in
    if (List.mem type1 type_list) then
      (stm_temp := s::!stm_temp;)
    else
      ()
  in
  List.iter helper stm_pool;
  !stm_temp

(* arbitrary steammon score/ranking algorithm. will tinker
   returns a list of (steammon, it's score) *)
let calculate_steammon_scores stm_lst =
 let score_temp = ref [] in
 let score_helper stm =
   let off_pts = (float_of_int stm.max_hp) /. 75.0 in    
   let def_pts = (float_of_int stm.defense) /. 100.0 in       
   let eff_pts =
     let eff_calc acc atk =
       let (e, chance) = atk.effect in
       match e with
       | Poisons -> acc +. (5.0 *. (float_of_int atk.accuracy) *. 
         (float_of_int chance)) /. 1000.0
       | Confuses -> acc +. (2.0 *. (float_of_int atk.accuracy) *. 
           (float_of_int chance)) /. 1000.0
       | Sleeps -> acc +. (7.0 *. (float_of_int atk.accuracy) *. 
             (float_of_int chance)) /. 1000.0
       | Paralyzes -> acc +. (5.0 *. (float_of_int atk.accuracy) *. 
               (float_of_int chance)) /. 1000.0
       | Freezes -> acc +. (4.0 *. (float_of_int atk.accuracy) *. 
                 (float_of_int chance)) /. 1000.0
       | _ -> acc
     in List.fold_left eff_calc 0.0 (get_attacks stm)
   in
   let atk_pts =
     let num_spl = ref 0 in
     let num_norm = ref 0 in      
     let atks = get_attacks stm in      
     let a_helper atk =
       if (List.mem atk.element spl_attack_types) then
         (num_spl := !num_spl + 1;)
       else
         (num_norm := !num_norm + 1;)
     in
     List.iter a_helper atks;
     if (!num_spl = 0) then (float_of_int stm.attack) /. 50.0
     else if (!num_norm = 0) then (float_of_int stm.spl_attack) /. 50.0
     else ((float_of_int !num_spl) /. 4.0) *. (float_of_int stm.spl_attack) +.
      ((float_of_int !num_norm) /. 4.0) *. (float_of_int stm.attack) /. 50.0
   in
   let stab_pts =
     let stab_calc acc atk =
       if (atk.element = (desome_type (stm.first_type)) || (atk.element = desome_type (stm.second_type)))
       then (acc +. 10.0) 
       else 
         (acc)
     in List.fold_left stab_calc 0.0 (get_attacks stm)
   in    
   let spd_pts = (float_of_int stm.speed) /. 50.0
   in 
   score_temp := 
   (stm, off_pts +. def_pts +. eff_pts +. atk_pts +. stab_pts +. spd_pts)::!score_temp
 in 
 List.iter score_helper stm_lst;
 !score_temp

(* returns the highest scored steammon from a list in the form
   of [(smn1, score1);(stm2, score2)]*)
let best_steammon lst = 
  let default = fst (List.hd lst) in 
  let curr_best = ref (default, 0.0) in
  let test_one s =
    if (snd s > snd !curr_best) then (curr_best := s)
    else
      (curr_best := !curr_best)
  in
  List.iter test_one lst;
  fst (!curr_best)

(* returns the stm's best attack against the types given *)
let best_attack stm type1 type2 =
  let attacks = get_attacks stm in
  let possible_atks = List.filter (fun x -> x.pp_remaining > 0) attacks in
  let good_atks = ref [] in
  let helper atk =
    let beatables = get_strengths atk.element in
    if (List.mem type1 beatables) || (List.mem type2 beatables)
    then (good_atks := atk::!good_atks;)
    else 
      ();
  in List.iter helper possible_atks;
  if (List.length !good_atks > 0) then
    (List.hd !good_atks)
  else 
    (List.hd possible_atks)

let handle_request c r =
  match r with
    | StarterRequest(gs) ->
      let (team1, team2) = gs in
      let us = if c = Red then team1 else team2 in
      let (our_stm, our_items) = us in
      let possible_stm = List.filter (fun x -> x.curr_hp > 0) our_stm in
      let them = if c = Red then team2 else team1 in
      let (their_stm, _) = them in
      let fighter = List.hd their_stm in
      let fight_type = desome_type fighter.first_type in
      let good_choices = steammon_of_types [fight_type] our_stm in
      let better = List.filter (fun x -> x.curr_hp > 0) good_choices in
      if (List.length better > 0) then 
        let p = List.hd better in SelectStarter(p.species)
      else
        let q = List.hd possible_stm in SelectStarter(q.species)
            
    | PickRequest(c, gsd, atks, sp) ->
      let their_new_stm = get_new_steammon !opponents_steammon gsd c in
      let valid_types = valid_type_choices !opponents_steammon in
      let possible_stm = steammon_of_types valid_types sp in
      let scored_stm = calculate_steammon_scores possible_stm in
      let best_stm = best_steammon scored_stm in 
      opponents_steammon := List.append their_new_stm !opponents_steammon;
      PickSteammon(best_stm.species)

    | ActionRequest (gr) ->
      let (a1, b1) = gr in
      let my_team = if c = Red then a1 else b1 in
      let (mons, my_inv) = my_team in
      let my_fighter = List.hd mons in
      let my_type1 = desome_type my_fighter.first_type in
      let my_type2 = desome_type my_fighter.second_type in
      let opp = if c = Red then b1 else a1 in
      let opp_fighter = List.hd (fst opp) in
      let opp_type1 = desome_type opp_fighter.first_type in
      let opp_type2 = desome_type opp_fighter.second_type in
      let other_healthy = 
        List.filter (fun x -> ((x.curr_hp > 0) & (not (x.species = my_fighter.species)))) mons
      in      
      let bad_types = List.append (get_ineffectives my_type1) (get_ineffectives my_type2) in
      (match mons with
       | h::t ->
         if ((List.mem opp_type1 bad_types || List.mem opp_type2 bad_types) & 
            (List.length other_healthy > 1))
         then 
           let next_up = List.hd other_healthy in
           SwitchSteammon(next_up.species)
         else if (h.curr_hp < (h.max_hp / 3)) & (List.nth my_inv 1 > 0)
         then UseItem(MaxPotion, h.species)
         else if (List.nth my_inv 2 > 0) then
           let fainted = List.filter (fun x -> x.curr_hp <= 0) mons in
           if List.length fainted >= 3 then 
             let scores = calculate_steammon_scores fainted in
             let best = best_steammon scores in
             UseItem(Revive, best.species)
           else
             let atk = best_attack h opp_type1 opp_type2 in
             UseAttack(atk.name)
         else
           let atk = best_attack h opp_type1 opp_type2 in
           UseAttack(atk.name)      
       | _ -> failwith "Uh oh, you should always have steammon...")
       
	  | PickInventoryRequest (gr) ->
	    let num_mp = ((cINITIAL_CASH) / 2) / cCOST_MAXPOTION in
	    let num_rev = ((cINITIAL_CASH) / 2) / cCOST_REVIVE in     
	    PickInventory([0; num_mp; num_rev; 0; 0; 0; 0; 0])

let () = run_bot handle_request